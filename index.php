<?php

session_start();

?>

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>VWM</title>
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="http://code.highcharts.com/modules/exporting.js"></script>
</head>
<body>
	<div id="page-container">
		<header class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">MI-VMW - semestrální projekt - Jakub Štefan/Eliška Kuběnová</a>
				</div>
			</div>
		</header>
		<main>
			<h2>Zarovnávání DNA sekvencí převedených na časové řady</h2>
			<section id="form-section">
				<form class="form-horizontal" id="upload-form" method="POST" action="scripts/upload.php" enctype="multipart/form-data">
					<div class="form-group">
						<label class="dna-label">DNA sekvence:</label>
						<div class="col-sm-8 col-sm-offset-2">
							<input name="file" type="file" id="file-input" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">A:</label>
						<div class="col-sm-8">
							<input class="form-control" name="A-value" type="number" required>	
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">C:</label>
						<div class="col-sm-8">
							<input class="form-control" name="C-value" type="number" required>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">G:</label>
						<div class="col-sm-8">
							<input class="form-control" name="G-value" type="number" required>	
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2">T:</label>
						<div class="col-sm-8">
							<input class="form-control" name="T-value" type="number" required>
						</div>
					</div>
					<input class="btn btn-primary col-sm-8 col-sm-offset-2" type="submit" value="Zarovnat">
				</form>
			</section>
			<?php 
			if (isset($_SESSION["results"])) { ?>
			<section id="table-section">
				<p>Vstupní sekvence: </p>
				<h2 id="tested-sequence-name"><?php echo sprintf("%s", $_SESSION["sequence"]["name"]); ?></h2>
				<table id="distance-table">
					<thead>
						<tr>
							<th>Pořadí</th>
							<th>ID</th>
							<th>Jméno</th>
							<th>DTW Vzdálenost</th>	
						</tr>
					</thead>
					<tbody>
					<?php 
						foreach ($_SESSION["results"] as $key => $item) { ?>
							<tr id="<?php echo $key; ?>" data-result="<?php echo $item['resultValues']; ?>" data-source="<?php echo $item['sourceValues']; ?>">
								<td class="order"><?php echo $key + 1; ?></td>
								<td class="id"><?php echo $item["id"]; ?></td>
								<td class="name"><?php echo $item["name"]; ?></td>
								<td class="distance"><?php echo $item["minDist"]; ?></td>
							</tr>
						<?php } 
						session_destroy();
						?>		
					</tbody>
				</table>
			</section>
			<div id="chart-container" style="min-width: 310px; height: 400px;"></div>
		</main>
		<footer>
			<p><?php echo sprintf("Doba běhu: %f s.", $_SESSION["timeElapsed"]);?></p>
		</footer>
		<?php } ?>
	</div>
	<script src="js/bootstrap.js"></script>

	<script>
		$(function () {
			var resultData = JSON.parse("[" + $("#0").data("result") + "]");
			var sourceData = JSON.parse("[" + $("#0").data("source") + "]");
		    var chartOptions = {
		        chart: {
		        	renderTo: "chart-container",
		        	zoomType: "x"
		        },
		        title: {
		            text: 'Porovnání časových řad DNA sekvencí'
		        },
		        xAxis: {
		        	title: {
		        		text: "Počet bází"
		        	}
		        },
		        yAxis: {
		            title: {
		                text: 'Velikost součtu'
		            }
		        },
		        plotOptions: {
		            area: {
		                marker: {
		                    radius: 0.5
		                },
		                lineWidth: 1,
		                states: {
		                    hover: {
		                        lineWidth: 1
		                    }
		                },
		                threshold: null
		            }
		        },

		        series: [{
		            type: 'area',
		            name: $("#0 td.name").text(),
		            pointInterval: 1,
		            pointStart: 0,
		            data: resultData,
		            color: "#0040DB",
		          	fillColor: {
		                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
		                stops: [
	                        [0, "rgba(0,174,219,1)"],
	                        [1, "rgba(0,174,219,.3)"]
                    	]
                	}
		        }, 
		        {
		            type: 'area',
		            name: $("#tested-sequence-name").text(),
		            pointInterval: 1,
		            pointStart: 0,
		            data: sourceData,
					color: "#FF5725",
		          	fillColor: {
		                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
		                stops: [
	                        [0, "rgba(255,196,37,1)"],
	                        [1, "rgba(255,196,37,.3)"]
                    	]
                	}
		        }]
		    };

		    var chart = new Highcharts.Chart(chartOptions);

			$("tr").click(function(){
				var resultData = JSON.parse("[" + $(this).data("result") + "]");
				var sourceData = JSON.parse("[" + $(this).data("source") + "]");
				chart.series[0].update({name: $(this).children("td.name").text()});
				chart.series[0].setData(resultData);
				chart.series[1].setData(sourceData, true);
			});
	});
	</script>
</body>
</html>
