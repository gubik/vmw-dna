<?php

/* inicializace session */
session_start();

/* Vyparsuje prvni DNA sekvenci ze souboru a volitelne zapise do DB. */
function parseSequence($dbConnection, $saveInDB) {
	if (!$_FILES["file"]["error"]) {
		$file = file_get_contents($_FILES["file"]["tmp_name"]); 
		$DNAsequences = explode(">", $file);
		$name = substr($DNAsequences[1], strpos($DNAsequences[1], " "), strpos($DNAsequences[1], ",") - strpos($DNAsequences[1], " "));
		$sequence = substr($DNAsequences[1], strpos($DNAsequences[1], "\n") + 1);
		$sequence = preg_replace("/\n|\r/", '', $sequence);

		/* volitelny zapis do DB */
		if ($saveInDB) {
			$query = "INSERT INTO dna (name, content) VALUES ('" . $name . "', '" . $sequence. "');";
			$ret = $dbConnection->query($query);	
		}
	}
	return array("name" => $name, "seq" => $sequence);
}

/* Ohodnoti sekvenci prirazenim jednotlivych ohodnoceni bazi zadanymi do formulare */
function evaluate($sequence) {
	if ($_POST) {
		$aValue = $_POST["A-value"];
		$cValue = $_POST["C-value"];
		$gValue = $_POST["G-value"];
		$tValue = $_POST["T-value"];

		$evalSequence = array();

		/* Rozdeleni sekvence do pole */ 
		$sequence = str_split($sequence);

		foreach ($sequence as $key => $char) {
			switch ($char) {
				case "A": {
					if($key == 0) {
						$evalSequence[$key] = $aValue;
					} else {
						$evalSequence[$key] = $evalSequence[$key - 1] + $aValue;
					}
					break;	
				}
				case "C": {
					if($key == 0) {
						$evalSequence[$key] = $cValue;
					} else {
						$evalSequence[$key] = $evalSequence[$key - 1] + $cValue;
					}
					break;					
				}
				case "G": {
					if($key == 0) {
						$evalSequence[$key] = $gValue;
					} else {
						$evalSequence[$key] = $evalSequence[$key - 1] + $gValue;
					}
					break;
				}
				case "T": {
					if($key == 0) {
						$evalSequence[$key] = $tValue;
					} else {
						$evalSequence[$key] = $evalSequence[$key - 1] + $tValue;
					}
					break;
				}
				default:
					echo "Last char: " . $char;
					die("Bad char error.");
					break;
			}
		}
	}

	return $evalSequence;
}

/* Vraci vsechny sekvence ulozene v DB */
function getSequences($dbConnection) {
	$DNAsequences = array();
	$query = "SELECT * FROM dna;";
	$result = $dbConnection->query($query);
	$index = 0;
	while($sequence = $result->fetch_array()) {
		$DNAsequences[$index] = array("id" => $sequence["id"], "name" => $sequence["name"], "seq" => $sequence["content"]);
		$index++;
	}
	return $DNAsequences;
}

/* Pocita DTW vzdalenost mezi dvema sekvencemi */
function DTWDistance($seq1, $seq2) {
	$DTW = array(array());

	for ($i = 1; $i < sizeof($seq1); $i++) {
		$DTW[$i][0] = INF;
	}
	for ($j = 1; $j < sizeof($seq2); $j++) {
		$DTW[0][$j] = INF;
	}
	$DTW[0][0] = 0;

	for ($i = 1; $i < sizeof($seq1); $i++) {
		for ($j = 1; $j < sizeof($seq2); $j++) {
			$cost = abs($seq1[$i] - $seq2[$j]);
			$DTW[$i][$j] = $cost + min($DTW[$i - 1][$j], $DTW[$i][$j - 1], $DTW[$i - 1][$j - 1]);
		}
	}

	return $DTW[sizeof($seq1) - 1][sizeof($seq2) - 1];
}

/* 
 * Vypocita DTW mezi kazdou sekvenci v DB a vstupni sekvenci.
 * Vysledky seradi podle namerene vzdalenosti vzestupne a vrati nejpodobnejsich 10 sekvenci.
 */
function DTW($dbConnection, $sequence) {
	$minDist = INF;
	$DNAsequences = getSequences($dbConnection);

	$evalSequence = evaluate($sequence);

	$results = array();

	foreach ($DNAsequences as $sequence) {
		$testedSequence = evaluate($sequence["seq"]);
		$dist = DTWDistance($evalSequence, $testedSequence);
		array_push($results, array(
			"name" => $sequence["name"], 
			"id" => $sequence["id"], 
			"minDist" => $dist, 
			"resultValues" => implode(",", $testedSequence), 
			"sourceValues" => implode(",", $evalSequence))
		);
	}

	usort($results, function ($seqA, $seqB) {
		return $seqA["minDist"] - $seqB["minDist"];
	});

	return array_slice($results, 0, 10);
}

/* Inicializace DB pripojeni */
$dbConnection = mysqli_connect("localhost", "root", "", "vwm");
if (!$dbConnection) {
	die("Connection error." . mysqli_error());
}

/* Parsovani vstupni sekvence */
$sequence = parseSequence($dbConnection, false);
$_SESSION["sequence"] = $sequence;

$t1 = microtime(true);

/* Spusteni vypoctu a ulozeni do session */
$_SESSION["results"] = DTW($dbConnection, $sequence["seq"]);

$t2 = microtime(true);

/* Mereni doby behu */
$timeElapsed = $t2 - $t1;
$_SESSION["timeElapsed"] = $timeElapsed;

/* Ukonceni DB pripojeni */
mysqli_close($dbConnection);

/* Redirect */
header("Location: ../index.php");

